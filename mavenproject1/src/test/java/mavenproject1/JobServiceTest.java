/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mavenproject1;

import java.time.LocalDate;
import java.time.Month;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author admin
 */
public class JobServiceTest {
    
    public JobServiceTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of checkEnableTime method, of class JobService.
     */
    @Test
    public void testCheckEnableTimeTodayIsBetweenStartTimeAndEndTime() {
        // Arrange
        System.out.println("checkEnableTime");
        LocalDate startTime = LocalDate.of(2021, 1, 31);
        LocalDate endTime = LocalDate.of(2021, 2, 5);
        LocalDate today = LocalDate.of(2021, 2, 3);
        boolean expResult = true;
        // Act
        boolean result = JobService.checkEnableTime(startTime, endTime, today);
        //Assert
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCheckEnableTimeTodayIsBeforeStartTime() {
        // Arrange
        System.out.println("checkEnableTime");
        LocalDate startTime = LocalDate.of(2021, 1, 31);
        LocalDate endTime = LocalDate.of(2021, 2, 5);
        LocalDate today = LocalDate.of(2021, 1, 30);
        boolean expResult = false;
        // Act
        boolean result = JobService.checkEnableTime(startTime, endTime, today);
        //Assert
        assertEquals(expResult, result);
    }
    
    public void testCheckEnableTimeTodayIsAfterEndTime() {
        // Arrange
        System.out.println("checkEnableTime");
        LocalDate startTime = LocalDate.of(2021, 1, 31);
        LocalDate endTime = LocalDate.of(2021, 2, 5);
        LocalDate today = LocalDate.of(2021, 2, 8);
        boolean expResult = false;
        // Act
        boolean result = JobService.checkEnableTime(startTime, endTime, today);
        //Assert
        assertEquals(expResult, result);
    }
    
    public void testCheckEnableTimeTodayIsEqualStartTime() {
        // Arrange
        System.out.println("checkEnableTime");
        LocalDate startTime = LocalDate.of(2021, 1, 31);
        LocalDate endTime = LocalDate.of(2021, 2, 5);
        LocalDate today = LocalDate.of(2021, 1, 31);
        boolean expResult = true;
        // Act
        boolean result = JobService.checkEnableTime(startTime, endTime, today);
        //Assert
        assertEquals(expResult, result);
    }
    
    public void testCheckEnableTimeTodayIsEqualEndTime() {
        // Arrange
        System.out.println("checkEnableTime");
        LocalDate startTime = LocalDate.of(2021, 1, 31);
        LocalDate endTime = LocalDate.of(2021, 2, 5);
        LocalDate today = LocalDate.of(2021, 2, 5);
        boolean expResult = true;
        // Act
        boolean result = JobService.checkEnableTime(startTime, endTime, today);
        //Assert
        assertEquals(expResult, result);
    }
    
}
